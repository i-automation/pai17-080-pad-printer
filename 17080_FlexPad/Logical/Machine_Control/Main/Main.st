(*********************************************************************************
 * Copyright: I-Automation
 * Author:    Joseph Seymour
 * Created:   December 15, 2017/1:43 PM 
 *********************************************************************************
PROGRAM delevoped TO take main machine control. Primary functions are TO MOVE the
main dial, trigger stations appropriately, manage part shift register, and stop
machine operation in appropriate circumstances.*)

PROGRAM _INIT
//Initialize function blocks
	Blink_0.Period 		:= BLINK_TIME;
	Blink_0.DutyCycle 	:= BLINK_CYCLE;
	 
END_PROGRAM

PROGRAM _CYCLIC
(*Management of non-station or state dependent operations*)

//Activate air if door and E-Stop relays inactive
IF NOT(Machine.IO.DigI.Door_Relay_Open) AND NOT (Machine.IO.DigI.E_Stop_Open) THEN

 	Machine.IO.DigO.Air_Pressure_Enable	:= TRUE;

ELSE

	Machine.IO.DigO.Air_Pressure_Enable	:= FALSE;
	Machine.State						:= ERROR;

END_IF

IF HMI.Commands.Cycle_Stop THEN
	
	Machine.State						:= STOP;

END_IF

//Main machine control state machine
CASE Machine.State OF

	INIT://State to preprare machine for operation

		Machine.Prev_State								:= INIT;
		Machine.IO.DigO.Open_Stack_Light 				:= Blink_0.Out;

		IF NOT(Load_Unload_Station.Interface.Alarm OR Print_Station.Interface.Alarm 
			OR Reject_Station.Interface.Alarm OR Part_Verification_Station.Interface.Alarm 
				OR Inspection_Station.Interface.Alarm OR Dial.Interface.Alarm) THEN
			//Wait for machine startup time to allow stations to boot and report potential
			//errors.
			IF startup_timer.Q THEN

				Machine.State							:= WAIT;

			ELSE

				Machine.State								:= ERROR;

			END_IF


		END_IF

	WAIT://State to wait for stations to be ready for indexing and HMI command


		Machine.Prev_State								:= WAIT;
		Machine.IO.DigO.Open_Stack_Light				:= Blink_0.Out;
		Machine.IO.DigO.Red_Stack_Light					:= FALSE;
		//Check for error and alarm conditions
		IF Load_Unload_Station.Interface.Alarm OR Print_Station.Interface.Alarm 
			OR Reject_Station.Interface.Alarm OR Part_Verification_Station.Interface.Alarm 
				OR Inspection_Station.Interface.Alarm OR Dial.Interface.Alarm THEN

			Machine.State								:= ALARM;

		ELSIF HMI.Commands.Manual THEN
			
			Machine.State								:= MANUAL;
        //Check for all stations done and ready for index
		ELSIF Load_Unload_Station.Interface.Done AND Print_Station.Interface.Done 
			AND Reject_Station.Interface.Done AND Part_Verification_Station.Interface.Done 
				AND Inspection_Station.Interface.Done THEN
			//Wait for operator command
			IF HMI.Commands.Cycle_Start THEN

				//HMI.Commands.Cycle_Start					:= FALSE;
				Machine.IO.DigO.Lock_Doors 					:= TRUE;
				Machine.State								:= INDEX;
				Load_Unload_Station.Interface.Start			:= FALSE;
				Print_Station.Interface.Start 				:= FALSE;
				Reject_Station.Interface.Start 				:= FALSE;
				Part_Verification_Station.Interface.Start	:= FALSE;
				Inspection_Station.Interface.Start 			:= FALSE;

			END_IF

		ELSE
			//Hold commands for each station until alarm or completion
			Load_Unload_Station.Interface.Start			:= TRUE;
			Print_Station.Interface.Start 				:= TRUE;
			Reject_Station.Interface.Start 				:= TRUE;
			Part_Verification_Station.Interface.Start	:= TRUE;
			Inspection_Station.Interface.Start 			:= TRUE; 

		END_IF	

	INDEX://State to move the dial by one index

		Machine.Prev_State 								:= INDEX;
		Machine.IO.DigO.Open_Stack_Light 				:= TRUE;
		Machine.IO.DigO.Red_Stack_Light					:= FALSE;

		Dial.Interface.Start							:= TRUE;
		
		IF Dial.Interface.Done THEN

			Dial.Interface.Start						:= FALSE;
			//Increment part array and clear removed part
			FOR i := 1 TO NUM_PARTS  DO
				
				Part[i] := Part[i - 1];

			END_FOR

			memset(ADR(Part[NUM_PARTS]), 0, SIZEOF(Part[0]));
			
			Machine.State								:= WAIT;

		END_IF

	STOP: //State to stop machine at operator command

		Machine.IO.DigO.Open_Stack_Light				:= Blink_0.Out;
		Machine.IO.DigO.Red_Stack_Light					:= FALSE;
		Machine.IO.DigO.Lock_Doors						:= FALSE;

		IF Machine.Prev_State <> STOP THEN

			stop_return_state							:= Machine.Prev_State;
		
		END_IF

		Machine.Prev_State								:= STOP;

		IF HMI.Commands.Cycle_Start THEN

			Machine.State								:= stop_return_state;
		
		END_IF

	ALARM://State to wait for clearing of station alarms not treated as critical errors

		Machine.IO.DigO.Red_Stack_Light				:= Blink_0.Out;

		IF Machine.Prev_State <> ALARM THEN

			alarm_return_state							:= Machine.Prev_State;
		
		END_IF

		Machine.Prev_State								:= ALARM;

		IF HMI.Commands.Reset THEN

			Machine.State									:= alarm_return_state;
			//Reset station alarms if station alarm is active
			Load_Unload_Station.Interface.AlarmReset		:= Load_Unload_Station.Interface.Alarm;
			Part_Verification_Station.Interface.AlarmReset	:= Part_Verification_Station.Interface.Alarm;
			Print_Station.Interface.AlarmReset				:= Print_Station.Interface.Alarm;
			Inspection_Station.Interface.AlarmReset			:= Inspection_Station.Interface.Alarm;
			Reject_Station.Interface.AlarmReset				:= Reject_Station.Interface.Alarm;
		
		END_IF
		
	ERROR://State to handle errors and error recovery

		Machine.IO.DigO.Open_Stack_Light				:= FALSE;

		IF Machine.Prev_State <> ERROR THEN

			error_return_state							:= Machine.Prev_State;
		
		END_IF

		Machine.Prev_State								:= ERROR;

		IF NOT(Machine.IO.DigI.Door_Relay_Open) OR NOT (Machine.IO.DigI.E_Stop_Open) 
			OR HMI.Commands.Cycle_Stop THEN

			Machine.IO.DigO.Red_Stack_Light 			:= TRUE;
			Load_Unload_Station.Interface.Stop			:= TRUE;
			Print_Station.Interface.Stop 				:= TRUE;
			Reject_Station.Interface.Stop 				:= TRUE;
			Part_Verification_Station.Interface.Stop	:= TRUE;
			Inspection_Station.Interface.Stop 			:= TRUE;
			Dial.Interface.Stop							:= TRUE;

		ELSIF HMI.Commands.Reset THEN

			Machine.State								:= error_return_state;	
			HMI.Commands.Reset							:= FALSE;
			Load_Unload_Station.Interface.Stop			:= FALSE;
			Print_Station.Interface.Stop 				:= FALSE;
			Reject_Station.Interface.Stop 				:= FALSE;
			Part_Verification_Station.Interface.Stop	:= FALSE;
			Inspection_Station.Interface.Stop 			:= FALSE;
			Dial.Interface.Stop							:= FALSE;
			
		ELSE

			Machine.IO.DigO.Red_Stack_Light				:= Blink_0.Out;

		END_IF


	MANUAL://State to handle manual machine mode

		Machine.Prev_State								:= MANUAL;
 		Machine.IO.DigO.Open_Stack_Light				:= Blink_0.Out;
		Machine.IO.DigO.Red_Stack_Light					:= FALSE;
		Load_Unload_Station.Interface.Manual			:= TRUE;
		Print_Station.Interface.Manual					:= TRUE;
		Reject_Station.Interface.Manual 				:= TRUE;
		Part_Verification_Station.Interface.Manual		:= TRUE;
		Inspection_Station.Interface.Manual				:= TRUE;
		Dial.Interface.Manual							:= TRUE;

		IF NOT(HMI.Commands.Manual) THEN

			Load_Unload_Station.Interface.Manual		:= FALSE;
			Print_Station.Interface.Manual				:= FALSE;
			Reject_Station.Interface.Manual 			:= FALSE;
			Part_Verification_Station.Interface.Manual	:= FALSE;
			Inspection_Station.Interface.Manual			:= FALSE;
			Dial.Interface.Manual						:= FALSE;
			Machine.State								:= WAIT;

		END_IF

END_CASE

Blink_0.Enable := TRUE;
Blink_0();

startup_timer.IN := TRUE;
startup_timer.PT := STARTUP_TIME;
startup_timer();

END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

