(*********************************************************************************
 * Copyright: i-Automation 
 * Author:    Joseph Seymour
 * Created:   December 20, 2017/8:36 AM 
 *********************************************************************************
 This program serves to control the part verification station based on the signals
from and to the main program. As the simplest station this also serves as an example
of how the program interface signals are intended to be handled.*)
PROGRAM _INIT
	(* Insert code here *)
	 
END_PROGRAM

PROGRAM _CYCLIC
	//Wait for trigger from main program to begin processing
	IF Part_Verification_Station.Interface.Start AND NOT(Part_Verification_Station.Interface.Done) THEN

		IF Machine.IO.DigI.Part_Present THEN

			Part[PART_VERIFICATION_INDEX].Present	:= TRUE;

		ELSE

			Part[PART_VERIFICATION_INDEX].Present	:= FALSE;
	
		END_IF
		//Report completion to main program
		Part_Verification_Station.Interface.Done	:= TRUE;
	
	END_IF
	//Reset done signal and wait for next trigger
	IF NOT(Part_Verification_Station.Interface.Start) THEN

		Part_Verification_Station.Interface.Done	:= FALSE;

	END_IF

END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

