(********************************************************************
 * COPYRIGHT -- iAutomation
 ********************************************************************
 * PROGRAM: alarmmgr
 * File: alarmmgr.st
 * Author: ?
 * Created: ?
 * Version: 1.00.0
 * Modified Date:8/2/2016
 * Modified BY:phouston
 ********************************************************************
 * Implementation OF PROGRAM alarmmgr

 * Description:

	This task handles setting and resetting alarms, and defines alarm
	level.

 * Options:

 * Version History:

 ********************************************************************)
PROGRAM _CYCLIC
	IF (DataMgrInit) THEN
		
		Reset := (EDGEPOS(HMI.Commands.Reset));
		HMI.Commands.Reset := 0;
				
		// expected emergency stop circuit state
		AlarmMgr.EStopOK := NOT(Machine.IO.DigI.E_Stop_Open);
				 	
		CASE Step OF
		
			STEP_IDLE: //
				IF (Reset) THEN
					
					AlarmMgr.ResetInProgress := 1;
					
					memset(ADR(AlarmMgr.Banner), 0, SIZEOF(AlarmMgr.Banner) );
					
					// RESET LATCHED FAULTS
					
					// reset IO module faults
					FOR i := 0 TO (SIZEOF(IO_ModuleOK)/SIZEOF(IO_ModuleOK[0])-1) DO
						IF ((IO_ModuleOK[i]) OR (NOT IO_ModuleRequired[i])) THEN
							AlarmBits.Hardware[i] := 0;
						END_IF
					END_FOR
					
					Step := STEP_FAULT_RESET;
					
				END_IF

			STEP_FAULT_RESET:
				Machine.IO.DigO.Safety_Relay_Reset := 1;
				TON_Reset.IN := 1;
				
				IF (TON_Reset.Q) THEN
					TON_Reset.IN := 0;
					Machine.IO.DigO.Safety_Relay_Reset := 0;
					
					AlarmMgr.ResetInProgress := 0;
					
					Step := STEP_IDLE;
				END_IF						
		END_CASE

		// IO module status
		IF (TON_IOInit.Q) THEN
			AllIOInit := 1;  // flag for rest of program to know we have all IO modules 
			FOR i := 0 TO ((SIZEOF(IO_ModuleOK)/SIZEOF(IO_ModuleOK[0])) - 1) DO
				IF ((NOT IO_ModuleOK[i]) AND (IO_ModuleRequired[i])) THEN
					AlarmBits.Hardware[i] := 1;
					AllIOInit := 0;
				END_IF
			END_FOR	
		END_IF
		
		// emergency stop reset required
		AlarmBits.Application[1] 	:= NOT AlarmMgr.EStopOK;
		
		//Check if Machine is Homed
		AlarmBits.Application[2] 	:= FALSE;
		
		//Restart required
		AlarmBits.Application[3] 	:= AlarmMgr.RestartRequired;
		
		AlarmBits.Application[4] 	:= IOForce.data.ForcingEnabled;

		//Station Alarms
		AlarmBits.Application[7]	:= Load_Unload_Station.Interface.Alarm;

		AlarmBits.Application[8]	:= Part_Verification_Station.Interface.Alarm;

		AlarmBits.Application[9]	:= Print_Station.Interface.Alarm;

		AlarmBits.Application[10]	:= Inspection_Station.Interface.Alarm;

		AlarmBits.Application[11]	:= Reject_Station.Interface.Alarm;

		AlarmBits.Application[12]	:= Dial.Interface.Alarm;

		//Dissabled until SMTP is either activated or removed
		// SMTP Messages 
//		AlarmBits.Communication[0] := SMTP.Error[0];
//		AlarmBits.Communication[1] := SMTP.Error[1];
//		AlarmBits.Communication[2] := SMTP.Error[2];
//		AlarmBits.Communication[3] := SMTP.Error[3];
		
		// Internet Conection Messages
//		AlarmBits.Communication[4] := SMTP.Error[4];
		
		// Technology Guarding Check 
		AlarmBits.Application[5] := NOT GetGuardStatus.licenseOk;
		AlarmBits.Application[6] := (NOT GetGuardStatus.licenseOk) AND (DongleInfo.serNo = 0);
		
		// Reset Dongle Info if liscense error occurs incase dongle was removed
		IF EDGENEG(GetGuardStatus.licenseOk) THEN
			memset(ADR(DongleInfo),0,SIZEOF(DongleInfo));
		END_IF
		
		//Set the SDM application status		
		IF (AlarmMgr.ErrFatal) THEN
			SdmSetAppParam_0.appMode := sdm_APPMODE_ERROR; 
		ELSIF (AlarmMgr.ErrCritical) THEN
			SdmSetAppParam_0.appMode := sdm_APPMODE_WARNING; 
		ELSE
			SdmSetAppParam_0.appMode := sdm_APPMODE_OK; 
		END_IF
				
		//Check Fault Level Of Active Alarms
		AlarmMgr.ErrCritical := 0;
		AlarmMgr.ErrFatal := 0;
		FOR i := 0 TO MAX_ALARM_APP_BIT DO // Application Alarms
			IF (AlarmBits.Application[i]) THEN
				CASE FaultLevel.Application[i] OF
					FAULTLEVEL_CRITICAL:
						AlarmMgr.ErrCritical := 1;
						
					FAULTLEVEL_FATAL:
						AlarmMgr.ErrFatal := 1;
				END_CASE
			END_IF			
		END_FOR

		FOR i := 0 TO MAX_ALARM_AXIS_BIT DO // Axis Alarms
			IF (AlarmBits.Axis[i]) THEN
				CASE FaultLevel.Axis[i] OF
					FAULTLEVEL_CRITICAL:
						AlarmMgr.ErrCritical := 1;
						
					FAULTLEVEL_FATAL:
						AlarmMgr.ErrFatal := 1;
				END_CASE
			END_IF			
		END_FOR
		
		FOR i := 0 TO MAX_ALARM_COMM_BIT DO // Communication Alarms
			IF (AlarmBits.Communication[i]) THEN
				CASE FaultLevel.Communication[i] OF
					FAULTLEVEL_CRITICAL:
						AlarmMgr.ErrCritical := 1;
						
					FAULTLEVEL_FATAL:
						AlarmMgr.ErrFatal := 1;
				END_CASE
			END_IF			
		END_FOR
		
		FOR i := 0 TO MAX_ALARM_HARDWARE_BIT DO // Hardware Alarms
			IF (AlarmBits.Hardware[i]) THEN
				CASE FaultLevel.Hardware[i] OF
					FAULTLEVEL_CRITICAL:
						AlarmMgr.ErrCritical := 1;
						
					FAULTLEVEL_FATAL:
						AlarmMgr.ErrFatal := 1;
				END_CASE
			END_IF			
		END_FOR
		
		// Set Color of Banner Alarms		
		IF (NOT AlarmMgr.ErrCritical AND NOT AlarmMgr.ErrFatal) THEN
			AlarmBanner_CDP := 0 + 2*256;
		ELSE
			AlarmBanner_CDP := 0 + 51*256;
		END_IF
		
		//FUB calls
		SdmSetAppParam_0();
		TON_IOInit();
		TON_Reset();
		GetGuardStatus();
		GetGuardDongles();
	END_IF
	
	
	(***************PSEUDOCODE WRITTEN TO CHANGE BITMAP GROUP*******************)
	(*
	FOR i := 0 TO MAX_ALARM_BIT BY 1 DO
	IF (FaultLevel[i] = 2) AND (AlarmBits[i] = TRUE) THEN
	HMI.BannerBitmapIndex := 3; //Red Bell
	ELSIF (FaultLevel[i] = 1) AND (AlarmBits[i] = TRUE) THEN
	HMI.BannerBitmapIndex := 2; //Red Flag
	ELSIF (FaultLevel[i] = 0) AND (AlarmBits[i] = TRUE) THEN
	HMI.BannerBitmapIndex := 1; //Red Message
	END_IF

	HMI.PrevBitmapIndex := HMI.BannerBitmapIndex;

	//When acknowledged...
	IF (HMI.PrevBitmapIndex = 3) AND NOT (AlarmBits[i] = TRUE) THEN
	HMI.BannerBitmapIndex := 5;	//Black Bell
	ELSIF (HMI.PrevBitmapIndex = 2) AND NOT (AlarmBits[i] = TRUE) THEN
	HMI.BannerBitmapIndex := 4;	//Black Flag
	ELSIF (HMI.PrevBitmapIndex = 2) AND NOT (AlarmBits[i] = TRUE) THEN
	HMI.BannerBitmapIndex := 0;	//Black Message
	END_IF
	END_FOR
	*)
END_PROGRAM



PROGRAM _INIT

(* init program *)
//
//	IF ( (SIZEOF(AlarmBits)/SIZEOF(AlarmBits[0])) <> (SIZEOF(FaultLevel)/SIZEOF(FaultLevel[0])) ) THEN
//		ERRxfatal(0,0,ADR('range of AlarmBits <> range of FaultLevel'));
//	END_IF
//
//	IF ( (SIZEOF(TON_DriveFaultDelay)/SIZEOF(TON_DriveFaultDelay[0])) <> (SIZEOF(Axis)/SIZEOF(Axis[0])) ) THEN
//		ERRxfatal(0,0,ADR('range of TON_DriveFaultDelay <> range of Axis'));
//	END_IF
//		
	TON_Reset.PT := T#5s;

	TON_EStopDetectDelay.PT := T#50ms;
	TON_IOInit.PT := T#10s;
	TON_IOInit.IN := 1;

	AlarmMgr.PowerOnCycles := AlarmMgr.PowerOnCycles;
	AlarmMgr.OperatingHoursPP := AlarmMgr.OperatingHoursPP;
	AlarmMgr.BatteryStatusCPU := AlarmMgr.BatteryStatusCPU;
	AlarmMgr.TemperatureCPU := AlarmMgr.TemperatureCPU;

	AlarmMgr.TemperatureENV := AlarmMgr.TemperatureENV;
	AlarmBitDateTimeRecipeNameUser[0] := 1; // this sets the 'alarm' for the current date/time/user/recipe
	AlarmGroupFilterMode := 4; // this sets the alarm control filter mode to filter based on the group number
	AlarmPriorityFilter := 2; // Filter Level for the Alarm Priority
	AlarmGroupFilter := 1; // group number for our alarms
	
	//Set the SDM Application status
	SdmSetAppParam_0.enable  := 1; 
	SdmSetAppParam_0.Option  := sdmOPTION_VOLATILE;
	//	SdmSetAppParam_0.pLink   := ;  -- Available for future custom HTML page if clicking on the application 'petal' in SDM
	
	// Set Up Tech Guard Check
	GetGuardStatus.enable := TRUE;
	GetGuardDongles.enable := TRUE;
	GetGuardDongles.dongleInfoSize := SIZEOF(DongleInfo);
	GetGuardDongles.pDongleInfos := ADR(DongleInfo);
	
	// Setup Text System Funtions
	brsstrcpy(ADR(namespace),ADR('AxisConfig')); 
	
	FOR i := 0 TO MAX_AXIS_INDEX DO
		ParTexts[i].GetBasicText.Namespace 		:= ADR(namespace);
		ParTexts[i].GetLimitText.Namespace 		:= ADR(namespace);
		ParTexts[i].GetTuningText.Namespace 	:= ADR(namespace);
		ParTexts[i].GetBasicText.TextBufferSize	:= 50;
		ParTexts[i].GetLimitText.TextBufferSize	:= 50;
		ParTexts[i].GetTuningText.TextBufferSize:= 50;
		ParTexts[i].GetBasicText.TextBuffer 	:= ADR(BasicText[i]);
		ParTexts[i].GetLimitText.TextBuffer 	:= ADR(LimitText[i]);
		ParTexts[i].GetTuningText.TextBuffer	:= ADR(TuningText[i]);
		ParTexts[i].GetBasicText.SearchMode		:= arTEXTSYS_SEARCH_LANGUAGE_ONLY;
		ParTexts[i].GetLimitText.SearchMode		:= arTEXTSYS_SEARCH_LANGUAGE_ONLY;
		ParTexts[i].GetTuningText.SearchMode	:= arTEXTSYS_SEARCH_LANGUAGE_ONLY;
	END_FOR
	
	LanguageOld := 10;
(*
FaultLevel|   Class    | Description																								| Stop Behavior |
-----------------------------------------------------------------------------------------------------------------------------------------------------	
	0	  |	  Message  | These are not faults, but rather are messages.
	1     |  Critical  | These are the faults that should prevent the machine from running in auto but not prevent manual operation |	After Feed	|
    2     |   Fatal    | These are the faults that should stop any motion in progress and prevent the machine from even turning on  |	Immediate	|		
	
	NOTE 1: Define unused faults as fatal so if you add one and forget to classify it, it'll become immediately obvious
	NOTE 2: this list can be generated very rapdidly in Excel using the formula ="FaultLevel[" & ROW()-1 & "] := FAULTLEVEL_FATAL;"
	*)
	
//	FOR i := 0 TO ((SIZEOF(AlarmBits)/SIZEOF(AlarmBits[0]))-1) DO
//		FaultLevel[i] := FAULTLEVEL_FATAL;
//	END_FOR
	
	FaultLevel.Hardware[1] 	:= FAULTLEVEL_FATAL;		// IO fault
	FaultLevel.Hardware[2] 	:= FAULTLEVEL_FATAL;		// IO fault
	FaultLevel.Hardware[3] 	:= FAULTLEVEL_FATAL;		// IO fault
	FaultLevel.Hardware[4] 	:= FAULTLEVEL_FATAL;		// IO fault
	FaultLevel.Hardware[5] 	:= FAULTLEVEL_FATAL;		// IO fault
	FaultLevel.Hardware[6] 	:= FAULTLEVEL_FATAL;		// IO fault
	FaultLevel.Hardware[7] 	:= FAULTLEVEL_FATAL;		// IO fault

	FaultLevel.Application[0]	:= FAULTLEVEL_MESSAGE;	// No Faults Message	
	FaultLevel.Application[1]	:= FAULTLEVEL_FATAL;	// estop reset required
	FaultLevel.Application[2]	:= FAULTLEVEL_CRITICAL;	// Machine Not Homed
	FaultLevel.Application[3]	:= FAULTLEVEL_FATAL;	// Restart required	
	FaultLevel.Application[4]	:= FAULTLEVEL_CRITICAL;	// IO forcing enabled
	FaultLevel.Application[5]	:= FAULTLEVEL_CRITICAL; // Technology Guarding Error
	FaultLevel.Application[6]	:= FAULTLEVEL_CRITICAL; // No Technology Guard Inserted
	FaultLevel.Application[7]	:= FAULTLEVEL_CRITICAL; // Load/Unload Station Alarm
	FaultLevel.Application[8]	:= FAULTLEVEL_CRITICAL; // Verification Station Alarm
	FaultLevel.Application[9]	:= FAULTLEVEL_CRITICAL; // Print Station Alarm
	FaultLevel.Application[10]	:= FAULTLEVEL_CRITICAL; // Inspection Station Alarm
	FaultLevel.Application[11]	:= FAULTLEVEL_CRITICAL; // Reject Station Alarm
	FaultLevel.Application[12]	:= FAULTLEVEL_CRITICAL; // Dial Alarm
	
	// SMTP Errors
	FaultLevel.Communication[0] := FAULTLEVEL_MESSAGE; 
	FaultLevel.Communication[1] := FAULTLEVEL_MESSAGE;
	FaultLevel.Communication[2] := FAULTLEVEL_MESSAGE;
	FaultLevel.Communication[3] := FAULTLEVEL_MESSAGE;
	
	//Network Communication Error
	FaultLevel.Communication[4] := FAULTLEVEL_MESSAGE;
	
END_PROGRAM
