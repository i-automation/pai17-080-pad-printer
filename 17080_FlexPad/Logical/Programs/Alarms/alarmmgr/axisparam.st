(********************************************************************
 * COPYRIGHT -- iAutomation
 ********************************************************************
 * PROGRAM: alarmmgr
 * File: alarmmgr.st
 * Author: 8/2/2016
 * Created: phouston
 * Version: 1.00.0
 * Modified Date:8/2/2016
 * Modified BY:phouston
 ********************************************************************
 * Implementation OF axisparam Action

 * Description:
	This ACTION is used FOR checking the parameters OF all the axes.
	Axis parameter ranges are checked based on the axis type AND an 
	error text is assigned using the ArTextSys Library	

 * Version History: V1.00 created 8/2/2016

 ********************************************************************)

ACTION AxisParameterCheck: 
	
	// Set language for Drive Faults
	IF HMI.Language.Current <> LanguageOld THEN
		IF HMI.Language.Current = 0  THEN
			FOR i := 0 TO 3 DO 
				ParTexts[i].GetBasicText.LanguageCode 	:= Language[HMI.Language.Current];
				ParTexts[i].GetLimitText.LanguageCode 	:= Language[HMI.Language.Current];
				ParTexts[i].GetTuningText.LanguageCode 	:= Language[HMI.Language.Current];
			END_FOR
		END_IF
		LanguageOld := HMI.Language.Current;
	END_IF
	
	// drive parameters & drive faults
	FOR i := 0 TO MAX_AXIS_INDEX DO
			
		IF (NOT SystemSettings.AxisConfig[i].Disabled) THEN 
			// axis NOT initialized faults
			AlarmBits.Axis[i*6+1] :=  (NOT Axis[i].data.Init);// AND (Axis[i].data.nc_obj_typ > 0);
	
			IF (Axis[i].data.nc_obj_typ = ncAXIS) THEN
				// only need to check tuning/limits for axes that use the set point generator...
				// ...inverters, SDC, or SIM axis that requires motor (which is a real axis running in simulation)
				IF ( (Axis[i].data.acp_typ = ncACP_TYP_INV) OR 
					(Axis[i].data.acp_typ = ncACP_TYP_SDC) OR 
					((Axis[i].data.acp_typ = ncACP_TYP_SIM) AND (SystemSettings.AxisConfig[i].Motor.Type <> MOTOR_NOTREQUIRED)) ) THEN
					
					// axis parameters not set (encoder scaling, jog speed, default accel/decel) faults	
					// required by any axis that does set point generation
					CASE SystemSettings.AxisConfig[i].Motor.Type OF
						MOTOR_UNDEFINED:
							AlarmBits.Axis[i*6+2] := TRUE; 
							ParTexts[i].BasicTextID := 'Axis.Basic.Motor';
						MOTOR_AC:
							IF (SystemSettings.AxisConfig[i].Motor.Voltage < 115) OR (SystemSettings.AxisConfig[i].Motor.Voltage > 480) THEN
								AlarmBits.Axis[i*6+2] := TRUE; 
								ParTexts[i].BasicTextID := 'Axis.Basic.Voltage';
							ELSIF (SystemSettings.AxisConfig[i].Motor.Frequency < 50) OR (SystemSettings.AxisConfig[i].Motor.Frequency > 60) THEN
								AlarmBits.Axis[i*6+2] := TRUE; 
								ParTexts[i].BasicTextID := 'Axis.Basic.Frequency';	
							ELSIF (SystemSettings.AxisConfig[i].Motor.Current <= 0)THEN
								AlarmBits.Axis[i*6+2] := TRUE; 	
								ParTexts[i].BasicTextID := 'Axis.Basic.Current';
							ELSIF (SystemSettings.AxisConfig[i].Motor.Speed <= 0) THEN
								AlarmBits.Axis[i*6+2] := TRUE; 
								ParTexts[i].BasicTextID := 'Axis.Basic.Speed';
							ELSIF (SystemSettings.AxisConfig[i].Motor.PowerFactor <= 0) OR (SystemSettings.AxisConfig[i].Motor.PowerFactor >= 1) THEN
								AlarmBits.Axis[i*6+2] := TRUE; 
								ParTexts[i].BasicTextID := 'Axis.Basic.PowerFactor';
							ELSIF (SystemSettings.AxisConfig[i].Motor.Power <= 0) THEN
								AlarmBits.Axis[i*6+2] := TRUE; 
								ParTexts[i].BasicTextID := 'Axis.Basic.Power';
							ELSIF (SystemSettings.AxisConfig[i].JogSpeed <= 0) THEN
								AlarmBits.Axis[i*6+2] := TRUE; 
								ParTexts[i].BasicTextID := 'Axis.Basic.JogSpeedLow';
							ELSIF (SystemSettings.AxisConfig[i].JogSpeed > SystemSettings.AxisConfig[i].Limits.AXLIM_V) THEN
								AlarmBits.Axis[i*6+2] := TRUE; 
								ParTexts[i].BasicTextID := 'Axis.Basic.JogSpeedHigh';
							ELSIF (SystemSettings.AxisConfig[i].JogSpeedHigh <= SystemSettings.AxisConfig[i].JogSpeed) THEN
								AlarmBits.Axis[i*6+2] := TRUE; 
								ParTexts[i].BasicTextID := 'Axis.Basic.JogSpeedLowHigh';
							ELSIF (SystemSettings.AxisConfig[i].JogSpeedHigh > SystemSettings.AxisConfig[i].Limits.AXLIM_V) THEN
								AlarmBits.Axis[i*6+2] := TRUE; 
								ParTexts[i].BasicTextID := 'Axis.Basic.JogSpeedHighHigh';
							ELSIF (SystemSettings.AxisConfig[i].JogIncrement <= 0) THEN
								AlarmBits.Axis[i*6+2] := TRUE; 
								ParTexts[i].BasicTextID := 'Axis.Basic.JogIncrement';
							ELSIF (SystemSettings.AxisConfig[i].Limits.AXLIM_T_JOLT < 0) THEN
								AlarmBits.Axis[i*6+2] := TRUE;
								ParTexts[i].BasicTextID := 'Axis.Limit.Jolt';
							ELSIF (SystemSettings.AxisConfig[i].Limits.AXLIM_T_JOLT > 0.2) THEN
								AlarmBits.Axis[i*6+2] := TRUE;
								ParTexts[i].BasicTextID := 'Axis.Limit.JoltHigh';
							ELSE
								ParTexts[i].BasicTextID := 'Axis.Basic.Ok';
								AlarmBits.Axis[i*6+2] := FALSE; 
							END_IF
								
						ELSE 
							IF (SystemSettings.AxisConfig[i].Motor.ScalingEncoderCounts <= 0) THEN
								AlarmBits.Axis[i*6+2] := TRUE; 
								ParTexts[i].BasicTextID := 'Axis.Basic.EncoderCount';
							ELSIF (SystemSettings.AxisConfig[i].Motor.ScalingMotorRevs <= 0) THEN
								AlarmBits.Axis[i*6+2] := TRUE; 
								ParTexts[i].BasicTextID := 'Axis.Basic.Scaling';
							ELSIF (SystemSettings.AxisConfig[i].JogSpeed <= 0) THEN
								AlarmBits.Axis[i*6+2] := TRUE; 
								ParTexts[i].BasicTextID := 'Axis.Basic.JogSpeedLow';
							ELSIF (SystemSettings.AxisConfig[i].JogSpeed > SystemSettings.AxisConfig[i].Limits.AXLIM_V) THEN
								AlarmBits.Axis[i*6+2] := TRUE; 
								ParTexts[i].BasicTextID := 'Axis.Basic.JogSpeedHigh';
							ELSIF (SystemSettings.AxisConfig[i].JogSpeedHigh <= SystemSettings.AxisConfig[i].JogSpeed) THEN
								AlarmBits.Axis[i*6+2] := TRUE; 
								ParTexts[i].BasicTextID := 'Axis.Basic.JogSpeedLowHigh';
							ELSIF (SystemSettings.AxisConfig[i].JogSpeedHigh > SystemSettings.AxisConfig[i].Limits.AXLIM_V) THEN
								AlarmBits.Axis[i*6+2] := TRUE; 
								ParTexts[i].BasicTextID := 'Axis.Basic.JogSpeedHighHigh';
							ELSIF (SystemSettings.AxisConfig[i].JogIncrement <= 0) THEN
								AlarmBits.Axis[i*6+2] := TRUE; 
								ParTexts[i].BasicTextID := 'Axis.Basic.JogIncrement';								
							ELSIF (SystemSettings.AxisConfig[i].Accel <= 0) THEN
								AlarmBits.Axis[i*6+2] := TRUE; 
								ParTexts[i].BasicTextID := 'Axis.Basic.AccelerationLow';
							ELSIF (SystemSettings.AxisConfig[i].Accel > SystemSettings.AxisConfig[i].Limits.AXLIM_A) THEN
								AlarmBits.Axis[i*6+2] := TRUE; 
								ParTexts[i].BasicTextID := 'Axis.Basic.Acceleration';
							ELSIF (SystemSettings.AxisConfig[i].Decel <= 0) THEN
								AlarmBits.Axis[i*6+2] := TRUE; 
								ParTexts[i].BasicTextID := 'Axis.Basic.DecelerationLow';
							ELSIF (SystemSettings.AxisConfig[i].Decel > SystemSettings.AxisConfig[i].Limits.AXLIM_A) THEN
								ParTexts[i].BasicTextID := 'Axis.Basic.Deceleration';
							ELSIF (SystemSettings.AxisConfig[i].Limits.AXLIM_T_JOLT < 0) THEN
								AlarmBits.Axis[i*6+2] := TRUE;
								ParTexts[i].BasicTextID := 'Axis.Limit.Jolt';
							ELSIF (SystemSettings.AxisConfig[i].Limits.AXLIM_T_JOLT > 0.2) THEN
								AlarmBits.Axis[i*6+2] := TRUE;
								ParTexts[i].BasicTextID := 'Axis.Limit.JoltHigh';
							ELSE
								ParTexts[i].BasicTextID := 'Axis.Basic.Ok';
								AlarmBits.Axis[i*6+2] := FALSE; 
							END_IF
					END_CASE						
						
					// all inverter, sdc and sim axes w/ motor defined require velocity limits, accel limits and valid jolt time
					// also if the above, check for valid software limits for a finite axis and lag error limit						
					IF (SystemSettings.AxisConfig[i].Motor.Type <> MOTOR_UNDEFINED) THEN
						IF (SystemSettings.AxisConfig[i].Limits.AXLIM_V <= 0) THEN
							AlarmBits.Axis[i*6+3] := TRUE;
							ParTexts[i].LimitTextID := 'Axis.Limit.Velocity';
						ELSIF (SystemSettings.AxisConfig[i].Limits.AXLIM_A <= 0) THEN
							AlarmBits.Axis[i*6+3] := TRUE;
							ParTexts[i].LimitTextID := 'Axis.Limit.Acceleration';
						ELSIF ((Axis[i].data.UnitBasis = UNITBASIS_FINITE) AND 
							(SystemSettings.AxisConfig[i].Limits.AXLIM_POS_SW_END <= SystemSettings.AxisConfig[i].Limits.AXLIM_NEG_SW_END)) THEN
							AlarmBits.Axis[i*6+3] := TRUE;
							ParTexts[i].LimitTextID := 'Axis.Limit.End';
						ELSIF ((SystemSettings.AxisConfig[i].Motor.Type <> MOTOR_NOTREQUIRED) AND 
							(SystemSettings.AxisConfig[i].Motor.Type <> MOTOR_AC) AND 
							(SystemSettings.AxisConfig[i].Limits.AXLIM_DS_STOP <= 0)) THEN
							AlarmBits.Axis[i*6+3] := TRUE;
							ParTexts[i].LimitTextID := 'Axis.Limit.LagError';
						ELSE
							ParTexts[i].LimitTextID := 'Axis.Limit.Ok';
							AlarmBits.Axis[i*6+3] := FALSE;
						END_IF
					END_IF
						
					// check tuning parameters
						
					IF (SystemSettings.AxisConfig[i].Motor.Type <> MOTOR_UNDEFINED) 
						AND (SystemSettings.AxisConfig[i].Motor.Type <> MOTOR_NOTREQUIRED) 
						AND (SystemSettings.AxisConfig[i].Motor.Type <> MOTOR_AC) THEN
						IF (SystemSettings.AxisConfig[i].Tuning.POS_CTRL_KV <= 0) THEN
							AlarmBits.Axis[i*6+4] := TRUE;
							ParTexts[i].TuningTextID := 'Axis.Tuning.PositionGain';
						ELSIF (SystemSettings.AxisConfig[i].Tuning.SCTRL_KV <= 0) THEN
							AlarmBits.Axis[i*6+4] := TRUE;
							ParTexts[i].TuningTextID := 'Axis.Tuning.SpeedGain';
						ELSIF (SystemSettings.AxisConfig[i].Tuning.SCTRL_TN < 0) THEN
							AlarmBits.Axis[i*6+4] := TRUE;
							ParTexts[i].TuningTextID := 'Axis.Tuning.SpeedIntegrator';
						ELSIF (SystemSettings.AxisConfig[i].Tuning.SCTRL_T_FILTER < 0) THEN
							AlarmBits.Axis[i*6+4] := TRUE;
							ParTexts[i].TuningTextID := 'Axis.Tuning.Filter';
						ELSIF (SystemSettings.AxisConfig[i].Tuning.NOTCH_MODE AND (SystemSettings.AxisConfig[i].Tuning.ISQ_FILTER_A0 <= 0)) THEN
							AlarmBits.Axis[i*6+4] := TRUE;
							ParTexts[i].TuningTextID := 'Axis.Tuning.Notch';
						ELSIF(SystemSettings.AxisConfig[i].Tuning.NOTCH_MODE AND (SystemSettings.AxisConfig[i].Tuning.ISQ_FILTER_A1 <= 0)) THEN
							AlarmBits.Axis[i*6+4] := TRUE;
							ParTexts[i].TuningTextID := 'Axis.Tuning.Notch2';
						ELSIF (SystemSettings.AxisConfig[i].Tuning.FF_TORQUE_LOAD < 0) THEN
							AlarmBits.Axis[i*6+4] := TRUE;
							ParTexts[i].TuningTextID := 'Axis.Tuning.Torque';
						ELSIF (SystemSettings.AxisConfig[i].Tuning.FF_TORQUE_POS < 0) THEN
							AlarmBits.Axis[i*6+4] := TRUE;
							ParTexts[i].TuningTextID := 'Axis.Tuning.TorquePos';
						ELSIF (SystemSettings.AxisConfig[i].Tuning.FF_TORQUE_NEG < 0) THEN
							AlarmBits.Axis[i*6+4] := TRUE;
							ParTexts[i].TuningTextID := 'Axis.Tuning.TorqueNeg';
						ELSIF (SystemSettings.AxisConfig[i].Tuning.FF_KV_TORQUE < 0) THEN
							AlarmBits.Axis[i*6+4] := TRUE;
							ParTexts[i].TuningTextID := 'Axis.Tuning.TorqueGain';
						ELSIF (SystemSettings.AxisConfig[i].Tuning.FF_INERTIA < 0) THEN
							AlarmBits.Axis[i*6+4] := TRUE;
							ParTexts[i].TuningTextID := 'Axis.Tuning.Inertia';
						ELSE
							ParTexts[i].TuningTextID := 'Axis.Tuning.Ok';
							AlarmBits.Axis[i*6+4] := FALSE;
						END_IF
					END_IF
				END_IF 
			
			ELSIF (Axis[i].data.nc_obj_typ = ncV_AXIS) THEN
				// basic parameters
				IF (SystemSettings.AxisConfig[i].JogSpeed <= 0) THEN
					AlarmBits.Axis[i*6+2] := TRUE; 
					ParTexts[i].BasicTextID := 'Axis.Basic.JogSpeedLow';
				ELSIF (SystemSettings.AxisConfig[i].JogSpeedHigh <= SystemSettings.AxisConfig[i].JogSpeed) THEN
					AlarmBits.Axis[i*6+2] := TRUE; 
					ParTexts[i].BasicTextID := 'Axis.Basic.JogSpeedLowHigh';
				ELSIF (SystemSettings.AxisConfig[i].JogIncrement <= 0) THEN
					AlarmBits.Axis[i*6+2] := TRUE; 
					ParTexts[i].BasicTextID := 'Axis.Basic.JogIncrement';
				ELSIF (SystemSettings.AxisConfig[i].Accel <= 0) THEN
					AlarmBits.Axis[i*6+2] := TRUE; 
					ParTexts[i].BasicTextID := 'Axis.Basic.AccelerationLow';
				ELSIF (SystemSettings.AxisConfig[i].Decel <= 0) THEN
					AlarmBits.Axis[i*6+2] := TRUE; 
					ParTexts[i].BasicTextID := 'Axis.Basic.DecelerationLow';
				ELSIF (SystemSettings.AxisConfig[i].Limits.AXLIM_T_JOLT < 0) THEN
					AlarmBits.Axis[i*6+2] := TRUE;
					ParTexts[i].BasicTextID := 'Axis.Limit.Jolt';
				ELSIF (SystemSettings.AxisConfig[i].Limits.AXLIM_T_JOLT > 0.2) THEN
					AlarmBits.Axis[i*6+2] := TRUE;
					ParTexts[i].BasicTextID := 'Axis.Limit.JoltHigh';
				ELSE
					ParTexts[i].BasicTextID := 'Axis.Basic.Ok';
					AlarmBits.Axis[i*6+2] := FALSE; 
				END_IF
									
				// only limit parameter for a virtual axis is the jolt time
				ParTexts[i].LimitTextID := 'Axis.Limit.Ok';
				AlarmBits.Axis[i*6+3] := FALSE;
					
				// tuning N/A
				ParTexts[i].TuningTextID := 'Axis.Tuning.Ok';
				AlarmBits.Axis[i*6+4] := FALSE;
					
			END_IF // end OF axis type if-statement
		ELSE // axis is disabled
			AlarmBits.Axis[i*6+1] := 0;
			AlarmBits.Axis[i*6+2] := 0;
			AlarmBits.Axis[i*6+3] := 0;
			AlarmBits.Axis[i*6+4] := 0;
		END_IF	

		// filter out quickstop/drive enable lost faults if we expect them 	
		// give this a short time delay though since it can take a few IO scans for the expected input state to become current		
		IF (AlarmMgr.EStopOK) THEN
			TON_DriveFaultDelay[i].IN := (Axis[i].data.ErrorID <> 0);
		ELSE
			TON_DriveFaultDelay[i].IN := (Axis[i].data.ErrorID <> 0) AND (Axis[i].data.ErrorID <> 1011) AND 
			(Axis[i].data.ErrorID <> 6021) AND (Axis[i].data.ErrorID <> 6023) AND 
			(Axis[i].data.ErrorID <> 6058) AND (Axis[i].data.ErrorID <> 6059);
		END_IF
		TON_DriveFaultDelay[i].PT := T#10ms; // short delay to allow the e-stop signal to get from the IO to the PLC
		TON_DriveFaultDelay[i]();
			
		AlarmBits.Axis[i*6+5] := TON_DriveFaultDelay[i].Q;
			
		AlarmBits.Axis[i*6+6] := Axis[i].data.Error AND NOT TON_DriveFaultDelay[i].Q;
		
		ParTexts[i].GetTuningText.TextID := ADR(ParTexts[i].TuningTextID);
		ParTexts[i].GetLimitText.TextID := ADR(ParTexts[i].LimitTextID);
		ParTexts[i].GetBasicText.TextID := ADR(ParTexts[i].BasicTextID);
		
	END_FOR	
		
	// Toggle Enable on GetText to refresh text when done
	FOR i := 0 TO MAX_AXIS_INDEX DO
		IF ParTexts[i].GetBasicText.Done OR ParTexts[i].GetBasicText.Error = TRUE THEN
			ParTexts[i].GetBasicText.Execute := FALSE;
		ELSE
			ParTexts[i].GetBasicText.Execute := TRUE;
		END_IF
			
		IF ParTexts[i].GetLimitText.Done OR ParTexts[i].GetLimitText.Error = TRUE THEN
			ParTexts[i].GetLimitText.Execute := FALSE;
		ELSE
			ParTexts[i].GetLimitText.Execute := TRUE;
		END_IF
			
		IF ParTexts[i].GetTuningText.Done OR ParTexts[i].GetTuningText.Error = TRUE THEN
			ParTexts[i].GetTuningText.Execute := FALSE;
		ELSE
			ParTexts[i].GetTuningText.Execute := TRUE;
		END_IF
	END_FOR
	
	// FUB CALLS
	FOR i := 0 TO MAX_AXIS_INDEX DO
		ParTexts[i].GetBasicText();
		ParTexts[i].GetLimitText();
		ParTexts[i].GetTuningText();
	END_FOR
	
END_ACTION
