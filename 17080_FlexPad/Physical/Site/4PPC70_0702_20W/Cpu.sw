﻿<?xml version="1.0" encoding="utf-8"?>
<?AutomationStudio Version=4.3.3.196?>
<SwConfiguration CpuAddress="SL1" xmlns="http://br-automation.co.at/AS/SwConfiguration">
  <TaskClass Name="Cyclic#1">
    <Task Name="ncsdcctrl" Source="ncsdcctrl.prg" Memory="UserROM" Language="IEC" Debugging="true" />
  </TaskClass>
  <TaskClass Name="Cyclic#2" />
  <TaskClass Name="Cyclic#3" />
  <TaskClass Name="Cyclic#4">
    <Task Name="Main" Source="Machine_Control.Main.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Load_Unloa" Source="Machine_Control.Load_Unload.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Part_Verif" Source="Machine_Control.Part_Verification.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Print" Source="Machine_Control.Print.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Inspection" Source="Machine_Control.Inspection.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Reject" Source="Machine_Control.Reject.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Diagnostic" Source="Support.Diagnostics.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="Dial" Source="Machine_Control.Dial.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="alarmmgr" Source="Support.alarmmgr.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="alarmsmsg" Source="Support.alarmsmsg.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="buildinfo" Source="Support.buildinfo.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="datamgr" Source="Support.datamgr.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="defaults" Source="Support.defaults.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="sysdump" Source="Support.sysdump.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="varRead" Source="Support.varRead.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="hmimgr" Source="Support.hmimgr.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="hwdef_PPC7" Source="Machine_Control.hwdef_PPC70Safe.prg" Memory="UserROM" Language="IEC" Debugging="true" />
    <Task Name="ioforce_C7" Source="Machine_Control.ioforce_C70.prg" Memory="UserROM" Language="IEC" Debugging="true" />
  </TaskClass>
  <TaskClass Name="Cyclic#5" />
  <TaskClass Name="Cyclic#6" />
  <TaskClass Name="Cyclic#7" />
  <TaskClass Name="Cyclic#8">
    <Task Name="Simulation" Source="Support.Simulation.prg" Memory="UserROM" Language="IEC" Debugging="true" />
  </TaskClass>
  <NcDataObjects>
    <NcDataObject Name="acp10etxen" Source="acp10etxen.dob" Memory="UserROM" Language="Ett" />
    <NcDataObject Name="DialAxisa" Source="DialAxisobj.DialAxisa.dob" Memory="UserROM" Language="Apt" />
    <NcDataObject Name="DialAxisi" Source="DialAxisobj.DialAxisi.dob" Memory="UserROM" Language="Ax" />
  </NcDataObjects>
  <Binaries>
    <BinaryObject Name="TCData" Source="" Memory="SystemROM" Language="Binary" />
    <BinaryObject Name="udbdef" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="ashwd" Source="" Memory="SystemROM" Language="Binary" />
    <BinaryObject Name="sysconf" Source="" Memory="SystemROM" Language="Binary" />
    <BinaryObject Name="ashwac" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="asfw" Source="" Memory="SystemROM" Language="Binary" />
    <BinaryObject Name="arconfig" Source="" Memory="SystemROM" Language="Binary" />
    <BinaryObject Name="Role" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="iomap" Source="" Memory="UserROM" Language="Binary" />
    <BinaryObject Name="User" Source="" Memory="UserROM" Language="Binary" />
  </Binaries>
  <Libraries>
    <LibraryObject Name="standard" Source="Libraries.standard.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="MTBasics" Source="Libraries.MTBasics.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="brsystem" Source="Libraries.brsystem.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="MTTypes" Source="Libraries.MTTypes.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="sys_lib" Source="Libraries.sys_lib.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="runtime" Source="Libraries.runtime.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="asstring" Source="Libraries.asstring.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="operator" Source="Libraries.operator.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="astime" Source="Libraries.astime.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="AsIecCon" Source="Libraries.AsIecCon.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="Acp10man" Source="Libraries.Acp10man.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="Acp10par" Source="Libraries.Acp10par.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="NcGlobal" Source="Libraries.NcGlobal.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="Acp10sdc" Source="Libraries.Acp10sdc.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="Acp10_MC" Source="Libraries.Acp10_MC.lby" Memory="UserROM" Language="Binary" Debugging="true" />
    <LibraryObject Name="AsBrStr" Source="Libraries.AsBrStr.lby" Memory="UserROM" Language="Binary" Debugging="true" />
  </Libraries>
</SwConfiguration>